import {AbstractControl} from "@angular/forms";
// import {Control} from "@angular/common";

export class CustomValidators{

    static validateEmail(control:AbstractControl){
       // debugger;
        let email=control.value;
        
         var regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var valid = regEx.test(control.value);
        return valid ? null : { email: true };
    }
}