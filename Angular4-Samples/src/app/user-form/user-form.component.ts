import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CustomValidators } from "./../common/validators/custom.validators"
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from './../service/user.service';
import { User } from '../user-form/user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  form: FormGroup;
  user = new User();


  constructor(fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private service: UserService) {

    this.form = fb.group({
      user: fb.group({
        id: [""],
        name: ["", Validators.required],
        email: ["", CustomValidators.validateEmail],
        phone: []
      }),
      address: fb.group({
        street: [],
        suite: [],
        city: [],
        zipcode: []
      })
    })
  }

  get name() {
    return this.form.get("user.name");
  }

  get email() {
    return this.form.get("user.email");
  }

  ngOnInit() {
    var id;
    this.route.paramMap.subscribe(param => {

      id = +param.get("id");
      console.log("id :" + id);

      if (id == 0)
        return;

      this.service.get(id).subscribe(user => {
        this.user = user;
      });
    })
  }

  blur(name) {
    console.log(name);
  }

  save() {
    if (this.form.valid) {
      //debugger;
      //var id = this.form.value.user.id;
      var id = this.user.id;

      if (id) {
        alert("User Edited successfully.")
      }
      else {
        console.log("insert user");
        this.service.post(this.user).subscribe(response => {
          console.log(response);
          alert("User Inserted successfully.")

        });
      }

      this.router.navigate(["users"]);

    }
  }

}
