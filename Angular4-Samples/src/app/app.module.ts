import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { UsersComponent } from "./users/users.component";
import { PostsComponent } from "./posts/posts.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { DataService } from "./service/data.service";
import { UserService } from "./service/user.service";
import { HttpModule } from "@angular/http";
import { UserFormComponent } from './user-form/user-form.component';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    UsersComponent,
    PostsComponent,
    NotFoundComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: "",
        component: HomeComponent
      },
      {
        path: "users",
        component: UsersComponent
      },
      {
        path: "user/new",
        component: UserFormComponent
      },
      {
        path: "user/:id",
        component: UserFormComponent
      },
      {
        path: "posts",
        component: PostsComponent
      },
      {
        path: "**",
        component: NotFoundComponent
      },

    ])
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
