import { Component, OnInit } from '@angular/core';
import {UserService} from './../service/user.service';
import { Router } from "@angular/router";

@Component({
  // selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users:any[];

  constructor(private service:UserService,  private router: Router) { }

  ngOnInit() {
    this.service.getAll().subscribe(users=>this.users=users)
  }

  edit(id){
    this.router.navigate(["/user",id])
  }

  delete(id,index){
    this.service.delete(id).subscribe(response=>{
      debugger;
      alert("delete successfully");    
      this.users.splice(index,1);
    })
  }

}
