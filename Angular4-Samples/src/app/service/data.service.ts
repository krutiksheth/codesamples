import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(private url,private http:Http) { }

  getAll(){

    return this.
                http
                .get(this.url)
                .map(response=>response.json());                   
  }

  get(id){

    return this.
                http
                .get(this.url+"/"+id)
                .map(response=>response.json());                   
  }

  post(post:any){
    return this
                .http
                .post(this.url,JSON.stringify(post))
                .map(response=>response.json());

  }

  delete(id){
    return this.http.delete(this.url+"/"+id).map(response=>response.json());
  }
}
