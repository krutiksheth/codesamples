import { SampleAngularPage } from './app.po';

describe('sample-angular App', () => {
  let page: SampleAngularPage;

  beforeEach(() => {
    page = new SampleAngularPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
