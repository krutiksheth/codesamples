﻿using System.Collections.Generic;

namespace BethanyPieShop.Model
{
    public class HomeViewModel
    {
        public IEnumerable<Pie> PiesOfTheWeek { get; set; }

    }
}