﻿using System.Collections.Generic;

namespace BethanyPieShop.Model
{
    public class PieListViewModel
    {
        public IEnumerable<Pie> Pies { get; set; }
        public string CurrentCategory { get; set; }
    }
}