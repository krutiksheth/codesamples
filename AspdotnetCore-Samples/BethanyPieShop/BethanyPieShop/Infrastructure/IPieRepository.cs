﻿using System.Collections.Generic;
using BethanyPieShop.Model;

namespace BethanyPieShop.Infrastructure
{
    public interface IPieRepository
    {
        IEnumerable<Pie> Pies { get; }
        IEnumerable<Pie> PiesOfTheWeek { get; }
        Pie GetPieById(int pieId);
    }
}