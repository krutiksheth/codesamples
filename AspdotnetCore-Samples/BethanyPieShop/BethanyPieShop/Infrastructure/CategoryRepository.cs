﻿using System.Collections.Generic;
using BethanyPieShop.Model;

namespace BethanyPieShop.Infrastructure
{
    public class CategoryRepository:ICategoryRepository
    {
        private readonly AppDbContext _appDbContext;
        public IEnumerable<Category> Categories => _appDbContext.Categories;

        public CategoryRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
    }
}