﻿using System.Collections.Generic;
using BethanyPieShop.Model;

namespace BethanyPieShop.Infrastructure
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> Categories { get; }

    }
}