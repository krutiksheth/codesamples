﻿using System.Collections.Generic;
using System.Linq;
using BethanyPieShop.Model;
using Microsoft.EntityFrameworkCore;

namespace BethanyPieShop.Infrastructure
{
    public class PieRepository : IPieRepository
    {
        private readonly AppDbContext _appDbContext;

        public IEnumerable<Pie> Pies
        {
            get { return _appDbContext.Pies.Include(c => c.Category); }
        }

        public IEnumerable<Pie> PiesOfTheWeek
        {
            get { return _appDbContext.Pies.Include(c => c.Category).Where(p => p.IsPieOfTheWeek); }
        }

        public PieRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public Pie GetPieById(int pieId)
        {
            return _appDbContext.Pies.FirstOrDefault(z => z.PieId == pieId);
        }
    }
}
