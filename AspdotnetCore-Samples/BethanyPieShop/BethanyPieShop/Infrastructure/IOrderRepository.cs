﻿using BethanyPieShop.Model;

namespace BethanyPieShop.Infrastructure
{
    public interface IOrderRepository
    {
        void CreateOrder(Order order);
    }
}